# Secret Agent Man engine

This is an open source engine reimplementation of the DOS game **Secret Agent Man** entirely in pure C.

## Design

Vulkan based and separate game and graphic logic on different threads.

## Prerequisites

vulkan and SDL

You also need the original SAMxyz.GFX files from the original game as they cannot be a part of this release.

