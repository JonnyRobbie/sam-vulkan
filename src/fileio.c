#include "fileio.h"
#include <stdio.h>
#include <stdlib.h>

int
binFileRead (char *filename, struct BinFile *binfile)
{
	printf ("Reading %s\n", filename);
	FILE *file;
	file = fopen (filename, "rb");
	if (!file) {
		fprintf (stderr, "Could not open file\n");
		return 1;
	}

	// Get file length
	fseek (file, 0, SEEK_END);
	binfile->size = ftell (file);
	fseek (file, 0, SEEK_SET);

	// Allocate memory
	binfile->content = (unsigned char *)malloc (binfile->size);
	if (!binfile->content) {
		fprintf (stderr, "Memory error\n");
		fclose (file);
		return 2;
	}

	// Read file contents into buffer
	fread (binfile->content, binfile->size, 1, file);
	fclose (file);

	return 0;
}

int
freeBinFile (struct BinFile *binfile)
{
	free (binfile->content);
	return 0;
}

int
padModuleBin (struct BinFile *binfile)
{
	uint32_t oldSize = binfile->size;
	/*
	 *printf("%i\n", oldSize % 4);
	 */
	if (oldSize % 4 != 0) {
		binfile->content = (unsigned char *)realloc (
			binfile->content, oldSize + oldSize % 4);
		if (!binfile->content) {
			fprintf (stderr, "Memory failed to reallocate\n");
			return 1;
		}
		/*
		 *printf("Uncorrected shader size %i\n",
		 *binfile->size);
		 */
		binfile->size += (4 - oldSize % 4);
	}
	/*
	 *printf("Final shader size %i\n", binfile->size);
	 */
	return 0;
}
