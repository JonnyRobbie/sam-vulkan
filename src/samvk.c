#include "samvk.h"
#include "fileio.h"
#include "utils.h"
#include <SDL2/SDL_vulkan.h>
#include <inttypes.h>
#include <stdio.h>

const int MAX_FRAMES_IN_FLIGHT = 2;

int
SAMVkCreateSwapchain (struct SAMVk *samvk)
{
	uint32_t i;
	// Create swapchain
	printf ("Creating swapchain\n");
	VkSwapchainCreateInfoKHR createSwapchainInfo = {};
	createSwapchainInfo.sType = VK_STRUCTURE_TYPE_SWAPCHAIN_CREATE_INFO_KHR;
	createSwapchainInfo.surface = samvk->surface;

	createSwapchainInfo.minImageCount
		= samvk->suitableSwapChain[samvk->suitableDevice].imageCount;
	createSwapchainInfo.imageFormat
		= samvk->suitableSwapChain[samvk->suitableDevice].format.format;
	createSwapchainInfo.imageColorSpace
		= samvk->suitableSwapChain[samvk->suitableDevice]
	                  .format.colorSpace;
	createSwapchainInfo.imageExtent.width
		= samvk->suitableSwapChain[samvk->suitableDevice].extent.width;
	createSwapchainInfo.imageExtent.height
		= samvk->suitableSwapChain[samvk->suitableDevice].extent.height;
	createSwapchainInfo.imageArrayLayers = 1;
	createSwapchainInfo.imageUsage = VK_IMAGE_USAGE_COLOR_ATTACHMENT_BIT;
	if (samvk->queueSetSize == 1) {
		createSwapchainInfo.imageSharingMode
			= VK_SHARING_MODE_EXCLUSIVE;
		createSwapchainInfo.queueFamilyIndexCount = 0;
		createSwapchainInfo.pQueueFamilyIndices = NULL;
	} else {
		createSwapchainInfo.imageSharingMode
			= VK_SHARING_MODE_CONCURRENT;
		createSwapchainInfo.queueFamilyIndexCount = samvk->queueSetSize;
		createSwapchainInfo.pQueueFamilyIndices = samvk->queueSet;
	}
	createSwapchainInfo.preTransform
		= samvk->suitableSwapChain[samvk->suitableDevice].transform;
	createSwapchainInfo.compositeAlpha = VK_COMPOSITE_ALPHA_OPAQUE_BIT_KHR;
	createSwapchainInfo.clipped = VK_TRUE;
	createSwapchainInfo.presentMode
		= samvk->suitableSwapChain[samvk->suitableDevice].presentMode;
	createSwapchainInfo.oldSwapchain = VK_NULL_HANDLE;

	if (vkCreateSwapchainKHR (samvk->device, &createSwapchainInfo, NULL,
	                          &samvk->swapChain)
	    != VK_SUCCESS) {
		printf ("Failed to create swap chain!\n");
		return -8;
	}

	vkGetSwapchainImagesKHR (
		samvk->device, samvk->swapChain,
		&samvk->suitableSwapChain[samvk->suitableDevice].imageCount,
		NULL);

	samvk->swapChainImages = malloc (
		samvk->suitableSwapChain[samvk->suitableDevice].imageCount
		* sizeof (VkImage));
	if (samvk->swapChainImages == NULL) {
		printf ("Failed to allocate swapchainImages\n");
		return 2;
	}

	printf ("Swapchain images count: %i\n",
	        samvk->suitableSwapChain[samvk->suitableDevice].imageCount);
	vkGetSwapchainImagesKHR (
		samvk->device, samvk->swapChain,
		&samvk->suitableSwapChain[samvk->suitableDevice].imageCount,
		samvk->swapChainImages);

	// Create image views
	samvk->swapChainImageViews = malloc (
		sizeof (VkImageView)
		* samvk->suitableSwapChain[samvk->suitableDevice].imageCount);
	if (samvk->swapChainImageViews == NULL) {
		printf ("Failed to allocate swapChainImageViews\n");
		return 2;
	}

	printf ("Creating swapChain imageViews");
	for (i = 0;
	     i < samvk->suitableSwapChain[samvk->suitableDevice].imageCount;
	     i++) {
		printf ("  Creating imageView %i\n", i);
		VkImageViewCreateInfo createViewInfo = {};
		createViewInfo.sType = VK_STRUCTURE_TYPE_IMAGE_VIEW_CREATE_INFO;
		createViewInfo.image = samvk->swapChainImages[i];
		createViewInfo.viewType = VK_IMAGE_VIEW_TYPE_2D;
		createViewInfo.format
			= samvk->suitableSwapChain[samvk->suitableDevice]
		                  .format.format;
		createViewInfo.components.r = VK_COMPONENT_SWIZZLE_IDENTITY;
		createViewInfo.components.g = VK_COMPONENT_SWIZZLE_IDENTITY;
		createViewInfo.components.b = VK_COMPONENT_SWIZZLE_IDENTITY;
		createViewInfo.components.a = VK_COMPONENT_SWIZZLE_IDENTITY;
		createViewInfo.subresourceRange.aspectMask
			= VK_IMAGE_ASPECT_COLOR_BIT;
		createViewInfo.subresourceRange.baseMipLevel = 0;
		createViewInfo.subresourceRange.levelCount = 1;
		createViewInfo.subresourceRange.baseArrayLayer = 0;
		createViewInfo.subresourceRange.layerCount = 1;
		if (vkCreateImageView (samvk->device, &createViewInfo, NULL,
		                       &samvk->swapChainImageViews[i])
		    != VK_SUCCESS) {
			printf ("Failed to create image "
			        "views!\n");
			return -9;
		}
	}

	// Create render pass
	VkAttachmentDescription colorAttachment = {};
	colorAttachment.format
		= samvk->suitableSwapChain[samvk->suitableDevice].format.format;
	colorAttachment.samples = VK_SAMPLE_COUNT_1_BIT;
	colorAttachment.loadOp = VK_ATTACHMENT_LOAD_OP_CLEAR;
	colorAttachment.storeOp = VK_ATTACHMENT_STORE_OP_STORE;
	colorAttachment.initialLayout = VK_IMAGE_LAYOUT_UNDEFINED;
	colorAttachment.finalLayout = VK_IMAGE_LAYOUT_PRESENT_SRC_KHR;

	VkAttachmentReference colorAttachmentRef = {};
	colorAttachmentRef.attachment = 0;
	colorAttachmentRef.layout = VK_IMAGE_LAYOUT_COLOR_ATTACHMENT_OPTIMAL;

	VkSubpassDescription subpass = {};
	subpass.pipelineBindPoint = VK_PIPELINE_BIND_POINT_GRAPHICS;

	subpass.colorAttachmentCount = 1;
	subpass.pColorAttachments = &colorAttachmentRef;

	// Load shader bytecode
	printf ("Reading shader files\n");

	if (binFileRead ("shaders/vert.spv", &samvk->vertShader)
	    || binFileRead ("shaders/frag.spv", &samvk->fragShader)) {
		printf ("Error reading shader files");
		return -10;
	}
	padModuleBin (&samvk->vertShader);
	padModuleBin (&samvk->fragShader);

	printf ("Creating shader modules\n");
	VkShaderModuleCreateInfo createVertModuleInfo = {};
	createVertModuleInfo.sType
		= VK_STRUCTURE_TYPE_SHADER_MODULE_CREATE_INFO;
	createVertModuleInfo.codeSize = samvk->vertShader.size;
	createVertModuleInfo.pCode
		= ((const uint32_t *)samvk->vertShader.content);

	if (vkCreateShaderModule (samvk->device, &createVertModuleInfo, NULL,
	                          &samvk->vertShaderModule)
	    != VK_SUCCESS) {
		printf ("Failed to create shader module!");
		return -11;
	}

	VkShaderModuleCreateInfo createFragModuleInfo = {};
	createFragModuleInfo.sType
		= VK_STRUCTURE_TYPE_SHADER_MODULE_CREATE_INFO;
	createFragModuleInfo.codeSize = samvk->fragShader.size;
	createFragModuleInfo.pCode
		= ((const uint32_t *)samvk->fragShader.content);

	if (vkCreateShaderModule (samvk->device, &createFragModuleInfo, NULL,
	                          &samvk->fragShaderModule)
	    != VK_SUCCESS) {
		printf ("Failed to create shader module!");
		return -12;
	}

	VkPipelineShaderStageCreateInfo vertShaderStageInfo = {};
	vertShaderStageInfo.sType
		= VK_STRUCTURE_TYPE_PIPELINE_SHADER_STAGE_CREATE_INFO;
	vertShaderStageInfo.stage = VK_SHADER_STAGE_VERTEX_BIT;
	vertShaderStageInfo.module = samvk->vertShaderModule;
	vertShaderStageInfo.pName = "main";

	VkPipelineShaderStageCreateInfo fragShaderStageInfo = {};
	fragShaderStageInfo.sType
		= VK_STRUCTURE_TYPE_PIPELINE_SHADER_STAGE_CREATE_INFO;
	fragShaderStageInfo.stage = VK_SHADER_STAGE_FRAGMENT_BIT;
	fragShaderStageInfo.module = samvk->fragShaderModule;
	fragShaderStageInfo.pName = "main";

	VkPipelineShaderStageCreateInfo shaderStages[2]
		= { vertShaderStageInfo, fragShaderStageInfo };

	// Fixed shaders create infos
	printf ("Creating fixed shaders infos\n");
	VkPipelineVertexInputStateCreateInfo vertexInputInfo = {};
	vertexInputInfo.sType
		= VK_STRUCTURE_TYPE_PIPELINE_VERTEX_INPUT_STATE_CREATE_INFO;
	vertexInputInfo.vertexBindingDescriptionCount = 0;
	vertexInputInfo.pVertexBindingDescriptions = NULL; // Optional
	vertexInputInfo.vertexAttributeDescriptionCount = 0;
	vertexInputInfo.pVertexAttributeDescriptions = NULL; // Optional

	VkPipelineInputAssemblyStateCreateInfo inputAssembly = {};
	inputAssembly.sType
		= VK_STRUCTURE_TYPE_PIPELINE_INPUT_ASSEMBLY_STATE_CREATE_INFO;
	inputAssembly.topology = VK_PRIMITIVE_TOPOLOGY_TRIANGLE_LIST;
	inputAssembly.primitiveRestartEnable = VK_FALSE;

	VkViewport viewport = {};
	viewport.x = 0.0f;
	viewport.y = 0.0f;
	viewport.width = (float)samvk->suitableSwapChain[samvk->suitableDevice]
	                         .extent.width;
	viewport.height = (float)samvk->suitableSwapChain[samvk->suitableDevice]
	                          .extent.height;
	viewport.minDepth = 0.0f;
	viewport.maxDepth = 1.0f;

	VkRect2D scissor = {};
	scissor.offset.x = 0;
	scissor.offset.y = 0;
	scissor.extent = samvk->suitableSwapChain[samvk->suitableDevice].extent;

	VkPipelineViewportStateCreateInfo viewportState = {};
	viewportState.sType
		= VK_STRUCTURE_TYPE_PIPELINE_VIEWPORT_STATE_CREATE_INFO;
	viewportState.viewportCount = 1;
	viewportState.pViewports = &viewport;
	viewportState.scissorCount = 1;
	viewportState.pScissors = &scissor;

	VkPipelineRasterizationStateCreateInfo rasterizer = {};
	rasterizer.sType
		= VK_STRUCTURE_TYPE_PIPELINE_RASTERIZATION_STATE_CREATE_INFO;
	rasterizer.depthClampEnable = VK_FALSE;
	rasterizer.rasterizerDiscardEnable = VK_FALSE;
	rasterizer.polygonMode = VK_POLYGON_MODE_FILL;
	rasterizer.lineWidth = 1.0f;
	rasterizer.cullMode = VK_CULL_MODE_BACK_BIT;
	rasterizer.frontFace = VK_FRONT_FACE_CLOCKWISE;
	rasterizer.depthBiasEnable = VK_FALSE;
	rasterizer.depthBiasConstantFactor = 0.0f; // Optional
	rasterizer.depthBiasClamp = 0.0f;          // Optional
	rasterizer.depthBiasSlopeFactor = 0.0f;    // Optional

	VkPipelineMultisampleStateCreateInfo multisampling = {};
	multisampling.sType
		= VK_STRUCTURE_TYPE_PIPELINE_MULTISAMPLE_STATE_CREATE_INFO;
	multisampling.sampleShadingEnable = VK_FALSE;
	multisampling.rasterizationSamples = VK_SAMPLE_COUNT_1_BIT;
	multisampling.minSampleShading = 1.0f;          // Optional
	multisampling.pSampleMask = NULL;               // Optional
	multisampling.alphaToCoverageEnable = VK_FALSE; // Optional
	multisampling.alphaToOneEnable = VK_FALSE;      // Optional

	VkPipelineColorBlendAttachmentState colorBlendAttachment = {};
	colorBlendAttachment.colorWriteMask
		= VK_COLOR_COMPONENT_R_BIT | VK_COLOR_COMPONENT_G_BIT
	          | VK_COLOR_COMPONENT_B_BIT | VK_COLOR_COMPONENT_A_BIT;
	colorBlendAttachment.blendEnable = VK_FALSE;

	VkPipelineColorBlendStateCreateInfo colorBlending = {};
	colorBlending.sType
		= VK_STRUCTURE_TYPE_PIPELINE_COLOR_BLEND_STATE_CREATE_INFO;
	colorBlending.logicOpEnable = VK_FALSE;
	colorBlending.logicOp = VK_LOGIC_OP_COPY; // Optional
	colorBlending.attachmentCount = 1;
	colorBlending.pAttachments = &colorBlendAttachment;
	colorBlending.blendConstants[0] = 0.0f; // Optional
	colorBlending.blendConstants[1] = 0.0f; // Optional
	colorBlending.blendConstants[2] = 0.0f; // Optional
	colorBlending.blendConstants[3] = 0.0f; // Optional

	VkPipelineLayoutCreateInfo pipelineLayoutInfo = {};
	pipelineLayoutInfo.sType
		= VK_STRUCTURE_TYPE_PIPELINE_LAYOUT_CREATE_INFO;
	pipelineLayoutInfo.setLayoutCount = 0;         // Optional
	pipelineLayoutInfo.pSetLayouts = NULL;         // Optional
	pipelineLayoutInfo.pushConstantRangeCount = 0; // Optional
	pipelineLayoutInfo.pPushConstantRanges = NULL; // Optional

	if (vkCreatePipelineLayout (samvk->device, &pipelineLayoutInfo, NULL,
	                            &samvk->pipelineLayout)
	    != VK_SUCCESS) {
		printf ("Failed to create pipeline layout!");
		return -13;
	}

	VkSubpassDependency dependency = {};
	dependency.srcSubpass = VK_SUBPASS_EXTERNAL;
	dependency.dstSubpass = 0;
	dependency.srcStageMask = VK_PIPELINE_STAGE_COLOR_ATTACHMENT_OUTPUT_BIT;
	dependency.srcAccessMask = 0;
	dependency.dstStageMask = VK_PIPELINE_STAGE_COLOR_ATTACHMENT_OUTPUT_BIT;
	dependency.dstAccessMask = VK_ACCESS_COLOR_ATTACHMENT_WRITE_BIT;

	VkRenderPassCreateInfo renderPassInfo = {};
	renderPassInfo.sType = VK_STRUCTURE_TYPE_RENDER_PASS_CREATE_INFO;
	renderPassInfo.attachmentCount = 1;
	renderPassInfo.pAttachments = &colorAttachment;
	renderPassInfo.subpassCount = 1;
	renderPassInfo.pSubpasses = &subpass;
	renderPassInfo.dependencyCount = 1;
	renderPassInfo.pDependencies = &dependency;

	if (vkCreateRenderPass (samvk->device, &renderPassInfo, NULL,
	                        &samvk->renderPass)
	    != VK_SUCCESS) {
		printf ("Failed to create render pass!");
		return -14;
	}

	VkGraphicsPipelineCreateInfo pipelineInfo = {};
	pipelineInfo.sType = VK_STRUCTURE_TYPE_GRAPHICS_PIPELINE_CREATE_INFO;
	pipelineInfo.stageCount = 2;
	pipelineInfo.pStages = shaderStages;
	pipelineInfo.pVertexInputState = &vertexInputInfo;
	pipelineInfo.pInputAssemblyState = &inputAssembly;
	pipelineInfo.pViewportState = &viewportState;
	pipelineInfo.pRasterizationState = &rasterizer;
	pipelineInfo.pMultisampleState = &multisampling;
	pipelineInfo.pDepthStencilState = NULL; // Optional
	pipelineInfo.pColorBlendState = &colorBlending;
	pipelineInfo.pDynamicState = NULL; // Optional
	pipelineInfo.layout = samvk->pipelineLayout;
	pipelineInfo.renderPass = samvk->renderPass;
	pipelineInfo.subpass = 0;
	pipelineInfo.basePipelineHandle = VK_NULL_HANDLE; // Optional
	pipelineInfo.basePipelineIndex = -1;              // Optional

	printf ("Creating pipeline\n");
	if (vkCreateGraphicsPipelines (samvk->device, VK_NULL_HANDLE, 1,
	                               &pipelineInfo, NULL,
	                               &samvk->graphicsPipeline)
	    != VK_SUCCESS) {
		printf ("Failed to create graphics pipeline!\n");
		return -15;
	}

	samvk->swapChainFramebuffers = malloc (
		sizeof (VkFramebuffer)
		* samvk->suitableSwapChain[samvk->suitableDevice].imageCount);
	if (samvk->swapChainFramebuffers == NULL) {
		printf ("Failed to allocate swapChainFramebuffers\n");
		return 3;
	}

	printf ("Creating swapChain framebuffers\n");
	for (i = 0;
	     i < samvk->suitableSwapChain[samvk->suitableDevice].imageCount;
	     i++) {
		printf ("  Creating framebuffer %i\n", i);
		VkImageView attachments[] = { samvk->swapChainImageViews[i] };

		VkFramebufferCreateInfo framebufferInfo = {};
		framebufferInfo.sType
			= VK_STRUCTURE_TYPE_FRAMEBUFFER_CREATE_INFO;
		framebufferInfo.renderPass = samvk->renderPass;
		framebufferInfo.attachmentCount = 1;
		framebufferInfo.pAttachments = attachments;
		framebufferInfo.width
			= samvk->suitableSwapChain[samvk->suitableDevice]
		                  .extent.width;
		framebufferInfo.height
			= samvk->suitableSwapChain[samvk->suitableDevice]
		                  .extent.height;
		framebufferInfo.layers = 1;

		if (vkCreateFramebuffer (samvk->device, &framebufferInfo, NULL,
		                         &samvk->swapChainFramebuffers[i])
		    != VK_SUCCESS) {
			printf ("failed to create framebuffer!");
			return -16;
		}
	}

	// Command pool
	printf ("Creating command pool\n");

	VkCommandPoolCreateInfo poolInfo = {};
	poolInfo.sType = VK_STRUCTURE_TYPE_COMMAND_POOL_CREATE_INFO;
	poolInfo.queueFamilyIndex = samvk->queueSet[0];
	poolInfo.flags = 0; // Optional

	if (vkCreateCommandPool (samvk->device, &poolInfo, NULL,
	                         &samvk->commandPool)
	    != VK_SUCCESS) {
		printf ("Failed to create command pool!\n");
		return -17;
	}

	samvk->commandBuffers = malloc (
		sizeof (VkCommandBuffer)
		* samvk->suitableSwapChain[samvk->suitableDevice].imageCount);
	if (samvk->commandBuffers == NULL) {
		printf ("Failed to allocate commandBuffers\n");
		return 4;
	}

	VkCommandBufferAllocateInfo allocInfo = {};
	allocInfo.sType = VK_STRUCTURE_TYPE_COMMAND_BUFFER_ALLOCATE_INFO;
	allocInfo.commandPool = samvk->commandPool;
	allocInfo.level = VK_COMMAND_BUFFER_LEVEL_PRIMARY;
	allocInfo.commandBufferCount
		= samvk->suitableSwapChain[samvk->suitableDevice].imageCount;

	if (vkAllocateCommandBuffers (samvk->device, &allocInfo,
	                              samvk->commandBuffers)
	    != VK_SUCCESS) {
		printf ("Failed to allocate command buffers!\n");
		return -18;
	}

	for (i = 0;
	     i < samvk->suitableSwapChain[samvk->suitableDevice].imageCount;
	     i++) {
		printf ("Recording command buffer for %ith image in "
		        "swapChain\n",
		        i);
		VkCommandBufferBeginInfo beginInfo = {};
		beginInfo.sType = VK_STRUCTURE_TYPE_COMMAND_BUFFER_BEGIN_INFO;
		beginInfo.flags = 0;               // Optional
		beginInfo.pInheritanceInfo = NULL; // Optional

		if (vkBeginCommandBuffer (samvk->commandBuffers[i], &beginInfo)
		    != VK_SUCCESS) {
			printf ("Failed to begin recording "
			        "command buffer!\n");
			return -19;
		}

		VkClearValue clearColor = { 0.0f, 0.0f, 0.0f, 1.0f };

		VkRenderPassBeginInfo renderPassInfo = {};
		renderPassInfo.sType = VK_STRUCTURE_TYPE_RENDER_PASS_BEGIN_INFO;
		renderPassInfo.renderPass = samvk->renderPass;
		renderPassInfo.framebuffer = samvk->swapChainFramebuffers[i];
		renderPassInfo.renderArea.offset.x = 0;
		renderPassInfo.renderArea.offset.y = 0;
		renderPassInfo.renderArea.extent
			= samvk->suitableSwapChain[samvk->suitableDevice]
		                  .extent;
		renderPassInfo.clearValueCount = 1;
		renderPassInfo.pClearValues = &clearColor;

		vkCmdBeginRenderPass (samvk->commandBuffers[i], &renderPassInfo,
		                      VK_SUBPASS_CONTENTS_INLINE);
		vkCmdBindPipeline (samvk->commandBuffers[i],
		                   VK_PIPELINE_BIND_POINT_GRAPHICS,
		                   samvk->graphicsPipeline);
		vkCmdDraw (samvk->commandBuffers[i], 3, 1, 0, 0);
		vkCmdEndRenderPass (samvk->commandBuffers[i]);

		if (vkEndCommandBuffer (samvk->commandBuffers[i])
		    != VK_SUCCESS) {
			printf ("Failed to record command "
			        "buffer!\n");
		}
	}
	return 0;
}

int
SAMVkRecreateSwapchain (struct SAMVk *samvk)
{
	vkDeviceWaitIdle (samvk->device);

	SAMVkCleanupSwapchain (samvk);

	SDL_GetWindowSize (samvk->gWin, &samvk->width, &samvk->height);

	SAMVkChooseSuitableSwapchain (samvk, samvk->suitableDevice,
	                              samvk->physDevice);
	if (SAMVkCreateSwapchain (samvk)) {
		printf ("Failed to create swapChain\n");
		return -8;
	}
	return 0;
}

int
SAMVkCleanupSwapchain (struct SAMVk *samvk)
{
	uint32_t i;

	for (i = 0;
	     i < samvk->suitableSwapChain[samvk->suitableDevice].imageCount;
	     i++) {
		vkDestroyFramebuffer (samvk->device,
		                      samvk->swapChainFramebuffers[i], NULL);
	}

	vkFreeCommandBuffers (
		samvk->device, samvk->commandPool,
		samvk->suitableSwapChain[samvk->suitableDevice].imageCount,
		samvk->commandBuffers);

	vkDestroyPipeline (samvk->device, samvk->graphicsPipeline, NULL);
	vkDestroyPipelineLayout (samvk->device, samvk->pipelineLayout, NULL);
	vkDestroyRenderPass (samvk->device, samvk->renderPass, NULL);

	vkDestroyCommandPool (samvk->device, samvk->commandPool, NULL);

	for (i = 0;
	     i < samvk->suitableSwapChain[samvk->suitableDevice].imageCount;
	     i++) {
		vkDestroyImageView (samvk->device,
		                    samvk->swapChainImageViews[i], NULL);
	}
	vkDestroySwapchainKHR (samvk->device, samvk->swapChain, NULL);

	free (samvk->commandBuffers);
	free (samvk->swapChainImageViews);
	free (samvk->swapChainImages);

	freeBinFile (&samvk->vertShader);
	freeBinFile (&samvk->fragShader);

	vkDestroyShaderModule (samvk->device, samvk->vertShaderModule, NULL);
	vkDestroyShaderModule (samvk->device, samvk->fragShaderModule, NULL);

	printf ("Swapchain cleaned up\n");

	return 0;
}

uint32_t
SAMVkChooseSuitableSwapchain (struct SAMVk *samvk, int32_t suitableDevice,
                              VkPhysicalDevice device)
{
	// Create a swapchain to a samvk structure member under the index of
	// suitableDevice and return 0 if there are no supported modes or
	// formats for that swapchain

	// Query details for swapchain support
	// swapcahin capabilities
	// chooseSwapExtent
	VkSurfaceCapabilitiesKHR capabilities;
	vkGetPhysicalDeviceSurfaceCapabilitiesKHR (device, samvk->surface,
	                                           &capabilities);
	printf ("    Swapchain current extent w:%i, h:%i\n",
	        capabilities.currentExtent.width,
	        capabilities.currentExtent.height);
	if (capabilities.currentExtent.width != UINT32_MAX) {
		samvk->suitableSwapChain[suitableDevice].extent.width
			= capabilities.currentExtent.width;
		samvk->suitableSwapChain[suitableDevice].extent.height
			= capabilities.currentExtent.height;
	} else {
		uint32_t width = samvk->width * samvk->pixelSize;
		uint32_t height = samvk->height * samvk->pixelSize;

		samvk->suitableSwapChain[suitableDevice].extent.width
			= max (capabilities.currentExtent.width,
		               min (capabilities.currentExtent.width, width));
		samvk->suitableSwapChain[suitableDevice].extent.height
			= max (capabilities.currentExtent.height,
		               min (capabilities.currentExtent.height, height));
	}
	samvk->suitableSwapChain[suitableDevice].imageCount
		= capabilities.minImageCount + 1;
	if (samvk->suitableSwapChain[suitableDevice].imageCount
	    > capabilities.maxImageCount) {
		samvk->suitableSwapChain[suitableDevice].imageCount
			= capabilities.maxImageCount;
	}
	samvk->suitableSwapChain[suitableDevice].transform
		= capabilities.currentTransform;

	// swapchain supported formats
	// chooseSwapSurfaceFormat
	uint32_t formatCount;
	uint32_t foundFormat = 0;
	vkGetPhysicalDeviceSurfaceFormatsKHR (device, samvk->surface,
	                                      &formatCount, NULL);
	printf ("    Swapchain supported formats: %i\n", formatCount);

	VkSurfaceFormatKHR formats[formatCount];
	vkGetPhysicalDeviceSurfaceFormatsKHR (device, samvk->surface,
	                                      &formatCount, formats);
	int32_t j;
	for (j = 0; j < formatCount; j++) {
		printf ("      Swapchain format %i and color space %i",
		        formats[j].format, formats[j].colorSpace);
		if (formats[j].format == VK_FORMAT_B8G8R8A8_SRGB
		    && formats[j].colorSpace
		               == VK_COLOR_SPACE_SRGB_NONLINEAR_KHR) {
			samvk->suitableSwapChain[suitableDevice].format.format
				= formats[j].format;
			samvk->suitableSwapChain[suitableDevice]
				.format.colorSpace
				= formats[j].colorSpace;
			foundFormat = 1;
			printf (" PREFERRED\n");
		} else {
			printf ("\n");
		}
	}
	if (!foundFormat) {
		samvk->suitableSwapChain[suitableDevice].format.format
			= formats[0].format;
		samvk->suitableSwapChain[suitableDevice].format.colorSpace
			= formats[0].colorSpace;
	}

	// swapchain supported modes
	// chooseSwapPresentMode
	uint32_t presentModeCount;
	uint32_t foundMode = 0;
	vkGetPhysicalDeviceSurfacePresentModesKHR (device, samvk->surface,
	                                           &presentModeCount, NULL);
	printf ("    Swapcahin supported modes: %i\n", presentModeCount);

	VkPresentModeKHR presentModes[presentModeCount];
	vkGetPhysicalDeviceSurfacePresentModesKHR (
		device, samvk->surface, &presentModeCount, presentModes);
	for (j = 0; j < presentModeCount; j++) {
		printf ("      Swapcahin mode %i", presentModes[j]);
		if (presentModes[j] == VK_PRESENT_MODE_MAILBOX_KHR) {
			samvk->suitableSwapChain[suitableDevice].presentMode
				= presentModes[j];
			foundMode = 1;
			printf (" PREFERRED\n");
		} else {
			printf ("\n");
		}
	}
	if (!foundMode) {
		samvk->suitableSwapChain[suitableDevice].presentMode
			= VK_PRESENT_MODE_FIFO_KHR;
	}

	return formatCount * presentModeCount;
}

int
SAMVkInit (struct SAMVk *samvk, SDL_Window *gWin, uint32_t width,
           uint32_t height, uint32_t pixelSize, float pixelRatio,
           int preferredDevice)
{
	uint32_t i, j, k;
	// Use validation layers
	const int nValidationLayers = 1;
	const char *validationLayers[] = { "VK_LAYER_KHRONOS_validation" };

	const uint32_t nDeviceExtensions = 1;
	const char *deviceExtensions[] = { VK_KHR_SWAPCHAIN_EXTENSION_NAME };

	samvk->width = width;
	samvk->height = height;
	samvk->pixelSize = pixelSize;
	samvk->pixelRatio = pixelRatio;

	samvk->gWin = gWin;

	samvk->framebufferResized = 0;

#ifdef NDEBUG
	const int enableValidationLayers = 0;
#else
	const int enableValidationLayers = 1;
#endif

	// Vulkan query for interface extensions
	uint32_t extCount;
	if (!SDL_Vulkan_GetInstanceExtensions (gWin, &extCount, NULL)) {
		printf ("Failed to get vulkan extension count");
		return -1;
	};
	printf ("Instance extension count: %i\n", extCount);
	const char *extNames[extCount];
	if (!SDL_Vulkan_GetInstanceExtensions (gWin, &extCount, extNames)) {
		printf ("Failed to get vulkan extension names");
		return -2;
	};
	for (int i = 0; i < extCount; i++) {
		printf ("  Extension %d: %s\n", i, extNames[i]);
	}

	// Check for available validation layers
	uint32_t vkValCount = 0;
	vkEnumerateInstanceLayerProperties (&vkValCount, NULL);

	if (enableValidationLayers) {
		printf ("Validation layers enabled\n");
	} else {
		printf ("Validation layers disabled\n");
	}

	printf ("Validation layers count: %i\n", vkValCount);
	VkLayerProperties vkValProp[vkValCount];
	vkEnumerateInstanceLayerProperties (&vkValCount, vkValProp);

	// Print the info about available layers
	for (i = 0; i < vkValCount; i++) {
		struct VkVersionNumber spec
			= getVersionFromVk (vkValProp[i].specVersion);
		struct VkVersionNumber impl
			= getVersionFromVk (vkValProp[i].implementationVersion);
		printf ("  Layer %d: %s ", i, vkValProp[i].layerName);
		printVersionFromVk (spec);
		printf ("/");
		printVersionFromVk (impl);
		printf (" - %s\n", vkValProp[i].description);
	}

	// Check availability of requested layers
	for (i = 0; i < nValidationLayers; i++) {
		int layerFound = 0;
		for (j = 0; j < vkValCount; j++) {
			if (!strcmp (validationLayers[i],
			             vkValProp[j].layerName)) {
				layerFound = 1;
				break;
			}
		}

		if (!layerFound && enableValidationLayers) {
			printf ("Failed to find a validation "
			        "layer");
			return -3;
		}
	}

	// Vulkan create application info structure
	printf ("Creating app info\n");
	VkApplicationInfo appInfo = {};
	appInfo.sType = VK_STRUCTURE_TYPE_APPLICATION_INFO;
	appInfo.pApplicationName = "SAM Reimplementation";
	appInfo.applicationVersion = VK_MAKE_VERSION (0, 1, 0);
	appInfo.pEngineName = "No Engine";
	appInfo.engineVersion = VK_MAKE_VERSION (0, 0, 0);
	appInfo.apiVersion = VK_API_VERSION_1_2;

	// Vulkan create instance info structure
	printf ("Creating instance info\n");
	VkInstanceCreateInfo createInfo = {};
	createInfo.sType = VK_STRUCTURE_TYPE_INSTANCE_CREATE_INFO;
	/*createInfo.sType =*/
	/*VK_STRUCTURE_TYPE_DEBUG_UTILS_MESSENGER_CREATE_INFO_EXT;*/
	createInfo.pApplicationInfo = &appInfo;
	createInfo.enabledExtensionCount = extCount;
	createInfo.ppEnabledExtensionNames = extNames;

	// Create info for validation layers
	VkDebugUtilsMessengerCreateInfoEXT debugInfo = {};
	if (enableValidationLayers) {
		printf ("Validation layers enabled\n");
		// TODO add a debug callback function
		/*debugInfo.sType =*/
		/*VK_STRUCTURE_TYPE_DEBUG_UTILS_MESSENGER_CREATE_INFO_EXT;*/
		debugInfo.messageSeverity
			= VK_DEBUG_UTILS_MESSAGE_SEVERITY_VERBOSE_BIT_EXT
		          | VK_DEBUG_UTILS_MESSAGE_SEVERITY_WARNING_BIT_EXT
		          | VK_DEBUG_UTILS_MESSAGE_SEVERITY_ERROR_BIT_EXT;
		debugInfo.messageType
			= VK_DEBUG_UTILS_MESSAGE_TYPE_GENERAL_BIT_EXT
		          | VK_DEBUG_UTILS_MESSAGE_TYPE_VALIDATION_BIT_EXT
		          | VK_DEBUG_UTILS_MESSAGE_TYPE_PERFORMANCE_BIT_EXT;
		debugInfo.pNext = NULL;

		createInfo.enabledLayerCount = nValidationLayers;
		createInfo.ppEnabledLayerNames = validationLayers;
		createInfo.pNext = &debugInfo;
		/*(VkDebugUtilsMessengerCreateInfoEXT*) &debugInfo;*/
	} else {
		printf ("Validation layers disabled\n");
		createInfo.enabledLayerCount = 0;
		createInfo.pNext = NULL;
	}

	// Create Vulkan instance
	printf ("Creating Vulkan instance\n");
	if (vkCreateInstance (&createInfo, NULL, &samvk->instance)
	    != VK_SUCCESS) {
		printf ("Failed to create a VK instance");
		return -4;
	}
	printf ("Instance created\n");

	// Vulkan query for interface extensions
	uint32_t vkExtCount;
	vkEnumerateInstanceExtensionProperties (NULL, &vkExtCount, NULL);
	printf ("Extension count: %i\n", vkExtCount);

	VkExtensionProperties vkExtProp[vkExtCount];
	vkEnumerateInstanceExtensionProperties (NULL, &vkExtCount, vkExtProp);

	for (i = 0; i < vkExtCount; i++) {
		struct VkVersionNumber spec
			= getVersionFromVk (vkExtProp[i].specVersion);
		printf ("  Extension %d: %s ", i, vkExtProp[i].extensionName);
		printVersionFromVk (spec);
		printf ("\n");
	}

	// Create a Vulkan surface
	printf ("Creating Vulkan surface\n");
	if (!SDL_Vulkan_CreateSurface (gWin, samvk->instance,
	                               &samvk->surface)) {
		printf ("Failed to create Vulkan surface");
		return -5;
	}

	// Select a physical device
	uint32_t deviceCount = 0;
	vkEnumeratePhysicalDevices (samvk->instance, &deviceCount, NULL);

	printf ("Physical devices found: %i\n", deviceCount);
	if (deviceCount == 0) {
		printf ("No Vulkan supported physical device was found.");
		return -6;
	}
	VkPhysicalDevice devices[deviceCount];
	vkEnumeratePhysicalDevices (samvk->instance, &deviceCount, devices);

	// Query properties and features of each physical devices
	samvk->suitableDevice = -1;
	uint32_t maxSuitabilityScore = 0;
	uint32_t deviceScore[deviceCount];

	struct SuitableQueueFamily {
		uint32_t graphicsFamily;
		uint32_t presentFamily;
		uint32_t existsGraphicsFamily;
		uint32_t existsPresentFamily;
	} suitableQueueFamily[deviceCount];

	samvk->suitableSwapChain
		= malloc (sizeof (struct SuitableSwapChain) * deviceCount);
	if (samvk->suitableSwapChain == NULL) {
		printf ("Failed to allocate memory for "
		        "suitableSwapChain\n");
		return 1;
	}

	for (i = 0; i < deviceCount; i++) {
		int deviceIsSuitable = 0;
		deviceScore[i] = 0;
		VkPhysicalDeviceProperties deviceProperties;
		VkPhysicalDeviceFeatures deviceFeatures;
		vkGetPhysicalDeviceProperties (devices[i], &deviceProperties);
		vkGetPhysicalDeviceFeatures (devices[i], &deviceFeatures);

		printf ("  Device %i: %s", i, deviceProperties.deviceName);

		if (deviceProperties.deviceType
		    == VK_PHYSICAL_DEVICE_TYPE_DISCRETE_GPU) {
			deviceScore[i] += 1000;
		}
		deviceScore[i] += deviceProperties.limits.maxImageDimension2D;

		// Query queue families
		uint32_t queueFamilyCount = 0;
		/*VkPhysicalDevice device = devices[i];*/

		vkGetPhysicalDeviceQueueFamilyProperties (
			devices[i], &queueFamilyCount, NULL);
		printf ("; queue familes count: %i\n", queueFamilyCount);
		VkQueueFamilyProperties queueFamilies[queueFamilyCount];
		vkGetPhysicalDeviceQueueFamilyProperties (
			devices[i], &queueFamilyCount, queueFamilies);

		suitableQueueFamily[i].existsGraphicsFamily = 0;
		suitableQueueFamily[i].existsPresentFamily = 0;

		// Check for queue support
		for (j = 0; j < queueFamilyCount; j++) {
			// Check queue family for required
			// features
			printf ("    Queue family %d, queues %i ", j,
			        queueFamilies[j].queueCount);
			printFlags (queueFamilies[j].queueFlags);
			printf ("\n");

			// Check if family is capable of
			// graphic queue
			if (queueFamilies[j].queueFlags
			    & VK_QUEUE_GRAPHICS_BIT) {
				printf ("      Graphic "
				        "queue "
				        "capable\n");
				if (!suitableQueueFamily[i]
				             .existsGraphicsFamily)
					suitableQueueFamily[i].graphicsFamily
						= j;
				suitableQueueFamily[i].existsGraphicsFamily = 1;
			}

			// Check if family is capable of
			// presentation queue
			VkBool32 presentSupport = 0;
			vkGetPhysicalDeviceSurfaceSupportKHR (
				devices[i], i, samvk->surface, &presentSupport);
			if (presentSupport) {
				printf ("      "
				        "Presentation "
				        "queue "
				        "capable\n");
				if (!suitableQueueFamily[i].existsPresentFamily)
					suitableQueueFamily[i].presentFamily
						= j;
				suitableQueueFamily[i].existsPresentFamily = 1;
			}
		}
		if (suitableQueueFamily[i].existsGraphicsFamily == 0
		    || suitableQueueFamily[i].existsPresentFamily == 0) {
			deviceScore[i] = 0;
		}

		// Check if device has a support for geometry shader
		if (!deviceFeatures.geometryShader) {
			deviceScore[i] = 0;
		}

		// check extension support
		uint32_t extensionCount;
		uint32_t extensionsSupported = 0;
		vkEnumerateDeviceExtensionProperties (devices[i], NULL,
		                                      &extensionCount, NULL);

		VkExtensionProperties availableExtensions[extensionCount];
		vkEnumerateDeviceExtensionProperties (
			devices[i], NULL, &extensionCount, availableExtensions);

		// Check availability of requested layers
		for (k = 0; k < nDeviceExtensions; k++) {
			int extensionFound = 0;
			for (j = 0; j < extensionCount; j++) {
				printf ("    Checking "
				        "requested "
				        "extension %s "
				        "against "
				        "available "
				        "extension %s\n",
				        deviceExtensions[k],
				        availableExtensions[j].extensionName);
				if (!strcmp (deviceExtensions[k],
				             availableExtensions[j]
				                     .extensionName)) {
					extensionFound = 1;
					extensionsSupported = 1;
					break;
				}
			}

			if (!extensionFound) {
				printf ("    Could not "
				        "find extension "
				        "for device %i\n",
				        i);
				deviceScore[i] = 0;
			}
		}

		if (extensionsSupported) {
			uint32_t formatMode = SAMVkChooseSuitableSwapchain (
				samvk, i, devices[i]);
			deviceScore[i] *= formatMode ? 1 : 0;
		}

		if (preferredDevice != -1 && preferredDevice == i) {
			deviceScore[i] *= 100;
		}
		printf ("    Device suitability score: %i\n", deviceScore[i]);
		if (deviceScore[i] > maxSuitabilityScore) {
			maxSuitabilityScore = deviceScore[i];
			samvk->suitableDevice = i;
		}
	}
	if (samvk->suitableDevice == -1) {
		printf ("No suitable physical device was found");
		return -7;
	} else {
		printf ("Found suitable device %i\n", samvk->suitableDevice);
	}
	samvk->physDevice = devices[samvk->suitableDevice];

	// Create a logical device
	float queuePriority = 1.0f;

	// Create a struct for other queueFamily
	samvk->queueSet[0]
		= suitableQueueFamily[samvk->suitableDevice].graphicsFamily;
	samvk->queueSet[1]
		= suitableQueueFamily[samvk->suitableDevice].presentFamily;

	samvk->queueSetSize
		= (sizeof (samvk->queueSet) / sizeof (samvk->queueSet[0]));
	printf ("Queried queue families %zu\n", samvk->queueSetSize);
	for (i = 0; i < samvk->queueSetSize; i++) {
		printf ("  Queried queue family %i on device %i\n",
		        samvk->queueSet[i], samvk->suitableDevice);
	}

	// Deduplicate the queue set
	for (i = 0; i < samvk->queueSetSize; i++) {
		for (j = i + 1; j < samvk->queueSetSize; j++) {
			if (samvk->queueSet[i] == samvk->queueSet[j]) {
				for (k = j; k < samvk->queueSetSize; k++) {
					samvk->queueSet[k]
						= samvk->queueSet[k + 1];
				}
				samvk->queueSetSize--;
				j--;
			}
		}
	}
	printf ("Unique queue families: %zu\n", samvk->queueSetSize);

	// Create an array for VkDeviceQueueCreateInfo
	VkDeviceQueueCreateInfo queueCreateInfos[samvk->queueSetSize];
	for (i = 0; i < samvk->queueSetSize; i++) {
		printf ("  Queue family %i on device %i\n", samvk->queueSet[i],
		        samvk->suitableDevice);
		queueCreateInfos[i].sType
			= VK_STRUCTURE_TYPE_DEVICE_QUEUE_CREATE_INFO;
		queueCreateInfos[i].queueFamilyIndex = samvk->queueSet[i];
		queueCreateInfos[i].queueCount = 1;
		queueCreateInfos[i].pQueuePriorities = &queuePriority;
		queueCreateInfos[i].flags = 0;
		queueCreateInfos[i].pNext = NULL;
	}

	VkPhysicalDeviceFeatures deviceFeatures = {};

	VkDeviceCreateInfo createDeviceInfo = {};
	createDeviceInfo.sType = VK_STRUCTURE_TYPE_DEVICE_CREATE_INFO;
	createDeviceInfo.pQueueCreateInfos = queueCreateInfos;
	createDeviceInfo.queueCreateInfoCount = samvk->queueSetSize;
	createDeviceInfo.pEnabledFeatures = &deviceFeatures;
	createDeviceInfo.enabledExtensionCount = nDeviceExtensions;
	createDeviceInfo.ppEnabledExtensionNames = deviceExtensions;
	if (enableValidationLayers) {
		createDeviceInfo.enabledLayerCount = nValidationLayers;
		createDeviceInfo.ppEnabledLayerNames = validationLayers;
	} else {
		createInfo.enabledLayerCount = 0;
	}

	printf ("Creating a logical device\n");
	samvk->result
		= vkCreateDevice (devices[samvk->suitableDevice],
	                          &createDeviceInfo, NULL, &samvk->device);
	if (samvk->result != VK_SUCCESS) {
		printf ("Error flags %i\n", samvk->result);
		printf ("Failed to create logical device!");
		return -7;
	}

	// Create queue handle
	printf ("Creating queue handles\n");
	vkGetDeviceQueue (
		samvk->device,
		suitableQueueFamily[samvk->suitableDevice].graphicsFamily, 0,
		&samvk->graphicsQueue);
	vkGetDeviceQueue (
		samvk->device,
		suitableQueueFamily[samvk->suitableDevice].presentFamily, 0,
		&samvk->presentQueue);

	if (SAMVkCreateSwapchain (samvk)) {
		printf ("Failed to create swapChain\n");
		return -8;
	}

	// Create sync objects
	printf ("Creating semaphores and fences\n");

	samvk->imageAvailableSemaphores
		= malloc (sizeof (VkSemaphore) * MAX_FRAMES_IN_FLIGHT);
	samvk->renderFinishedSemaphores
		= malloc (sizeof (VkSemaphore) * MAX_FRAMES_IN_FLIGHT);
	samvk->inFlightFences
		= malloc (sizeof (VkFence) * MAX_FRAMES_IN_FLIGHT);
	samvk->imagesInFlight = malloc (
		sizeof (VkFence)
		* samvk->suitableSwapChain[samvk->suitableDevice].imageCount);
	samvk->currentFrame = 0;

	for (i = 0;
	     i < samvk->suitableSwapChain[samvk->suitableDevice].imageCount;
	     i++) {
		samvk->imagesInFlight[i] = VK_NULL_HANDLE;
	}

	if (samvk->imageAvailableSemaphores == NULL
	    || samvk->renderFinishedSemaphores == NULL
	    || samvk->inFlightFences == NULL || samvk->imagesInFlight == NULL) {
		printf ("Failed to allocate sync objects!\n");
		return 5;
	}

	VkSemaphoreCreateInfo semaphoreInfo = {};
	semaphoreInfo.sType = VK_STRUCTURE_TYPE_SEMAPHORE_CREATE_INFO;

	VkFenceCreateInfo fenceInfo = {};
	fenceInfo.sType = VK_STRUCTURE_TYPE_FENCE_CREATE_INFO;
	fenceInfo.flags = VK_FENCE_CREATE_SIGNALED_BIT;

	for (i = 0; i < MAX_FRAMES_IN_FLIGHT; i++) {
		if (vkCreateSemaphore (samvk->device, &semaphoreInfo, NULL,
		                       &samvk->imageAvailableSemaphores[i])
		            != VK_SUCCESS
		    || vkCreateSemaphore (samvk->device, &semaphoreInfo, NULL,
		                          &samvk->renderFinishedSemaphores[i])
		               != VK_SUCCESS
		    || vkCreateFence (samvk->device, &fenceInfo, NULL,
		                      &samvk->inFlightFences[i])
		               != VK_SUCCESS) {

			printf ("Failed to create semaphores!\n");
		}
	}

	printf ("SAMVk initialized\n");
	return 0;
}

int
SAMVkDestruct (struct SAMVk *samvk)
{
	uint32_t i;
	// Cleanup phase
	printf ("Destroying evidence\n");

	SAMVkCleanupSwapchain (samvk);

	for (i = 0; i < MAX_FRAMES_IN_FLIGHT; i++) {
		vkDestroySemaphore (samvk->device,
		                    samvk->renderFinishedSemaphores[i], NULL);
		vkDestroySemaphore (samvk->device,
		                    samvk->imageAvailableSemaphores[i], NULL);
		vkDestroyFence (samvk->device, samvk->inFlightFences[i], NULL);
	}

	vkDestroyDevice (samvk->device, NULL);
	vkDestroySurfaceKHR (samvk->instance, samvk->surface, NULL);
	vkDestroyInstance (samvk->instance, NULL);

	free (samvk->imagesInFlight);
	free (samvk->inFlightFences);
	free (samvk->imageAvailableSemaphores);
	free (samvk->renderFinishedSemaphores);
	free (samvk->suitableSwapChain);

	printf ("SAMVk destroyed\n");
	return 0;
}

int
SAMVkDrawFrame (struct SAMVk *samvk)
{
	uint32_t imageIndex;

	vkWaitForFences (samvk->device, 1,
	                 &samvk->inFlightFences[samvk->currentFrame], VK_TRUE,
	                 UINT64_MAX);

	VkResult result = vkAcquireNextImageKHR (
		samvk->device, samvk->swapChain, UINT64_MAX,
		samvk->imageAvailableSemaphores[samvk->currentFrame],
		VK_NULL_HANDLE, &imageIndex);

	if (result == VK_ERROR_OUT_OF_DATE_KHR) {
		// recreate swapchain
		SAMVkRecreateSwapchain (samvk);
	} else if (result != VK_SUCCESS && result != VK_SUBOPTIMAL_KHR) {
		printf ("Failed to acquire swapCahin image\n");
		return 1;
	}

	// Check if a previous frame is using this image
	// (i.e. there is its fence to wait on)
	if (samvk->imagesInFlight[imageIndex] != VK_NULL_HANDLE) {
		vkWaitForFences (samvk->device, 1,
		                 &samvk->imagesInFlight[imageIndex], VK_TRUE,
		                 UINT64_MAX);
	}

	// Mark the image as now being in use by this frame
	samvk->imagesInFlight[imageIndex]
		= samvk->inFlightFences[samvk->currentFrame];

	VkSubmitInfo submitInfo = {};
	submitInfo.sType = VK_STRUCTURE_TYPE_SUBMIT_INFO;

	VkSemaphore waitSemaphores[]
		= { samvk->imageAvailableSemaphores[samvk->currentFrame] };
	VkPipelineStageFlags waitStages[]
		= { VK_PIPELINE_STAGE_COLOR_ATTACHMENT_OUTPUT_BIT };
	submitInfo.waitSemaphoreCount = 1;
	submitInfo.pWaitSemaphores = waitSemaphores;
	submitInfo.pWaitDstStageMask = waitStages;
	submitInfo.commandBufferCount = 1;
	submitInfo.pCommandBuffers = &samvk->commandBuffers[imageIndex];

	VkSemaphore signalSemaphores[]
		= { samvk->renderFinishedSemaphores[samvk->currentFrame] };
	submitInfo.signalSemaphoreCount = 1;
	submitInfo.pSignalSemaphores = signalSemaphores;

	vkResetFences (samvk->device, 1,
	               &samvk->inFlightFences[samvk->currentFrame]);

	if (vkQueueSubmit (samvk->graphicsQueue, 1, &submitInfo,
	                   samvk->inFlightFences[samvk->currentFrame])
	    != VK_SUCCESS) {
		printf ("Failed to submit draw command buffer!\n");
		return 3;
	}

	VkPresentInfoKHR presentInfo = {};
	presentInfo.sType = VK_STRUCTURE_TYPE_PRESENT_INFO_KHR;

	presentInfo.waitSemaphoreCount = 1;
	presentInfo.pWaitSemaphores = signalSemaphores;

	VkSwapchainKHR swapChains[] = { samvk->swapChain };
	presentInfo.swapchainCount = 1;
	presentInfo.pSwapchains = swapChains;
	presentInfo.pImageIndices = &imageIndex;

	result = vkQueuePresentKHR (samvk->presentQueue, &presentInfo);
	if (result == VK_ERROR_OUT_OF_DATE_KHR || result == VK_SUBOPTIMAL_KHR
	    || samvk->framebufferResized) {
		samvk->framebufferResized = 0;
		SAMVkRecreateSwapchain (samvk);
	} else if (result != VK_SUCCESS) {
		printf ("Failed to present swapCahin image\n");
		return 1;
	}

	vkQueueWaitIdle (samvk->presentQueue);

	samvk->currentFrame = (samvk->currentFrame + 1) % MAX_FRAMES_IN_FLIGHT;
	return 0;
}
