#ifndef FILEIO_H
#define FILEIO_H

#include <inttypes.h>

struct BinFile {
	uint32_t size;
	unsigned char *content;
};

int binFileRead (char *filename, struct BinFile *binfile);

int freeBinFile (struct BinFile *binfile);

int padModuleBin (struct BinFile *binfile);

#endif
