#ifndef UTILS_H
#define UTILS_H

#include <inttypes.h>
#include <stdint.h>

struct VkVersionNumber {
	uint32_t major;
	uint32_t minor;
	uint32_t patch;
};

struct VkVersionNumber getVersionFromVk (uint32_t version);

void printVersionFromVk (struct VkVersionNumber version);

void printFlags (uint32_t flags);

uint32_t min (uint32_t a, uint32_t b);

uint32_t max (uint32_t a, uint32_t b);

#endif
