#include "graphics.h"
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

const uint32_t CGApalette[16]
	= { 0x00000000, 0x55555500, 0x0000AA00, 0x5555FF00,
	    0x00AA0000, 0x55FF5500, 0x00AAAA00, 0x55FFFF00,
	    0xAA000000, 0xFF555500, 0xAA00AA00, 0xFF55FF00,
	    0xAA550000, 0xFFFF5500, 0xAAAAAA00, 0xFFFFFF00 };

void
resizeSet8 (struct tileset8 *tileset, uint32_t delta)
{
	if (tileset->n == 0) {
		printf ("Mallocing t8\n");
		tileset->n = delta;
		tileset->tiles = malloc (tileset->n * sizeof (struct tile8));
		if (!tileset->tiles) {
			printf ("Failed malloc\n");
			exit (1);
		}
	} else {
		tileset->n += delta;
		tileset->tiles = realloc (tileset->tiles,
		                          tileset->n * sizeof (struct tile8));
		if (!tileset->tiles) {
			printf ("Failed realloc\n");
			exit (1);
		}
	}
}

void
insertSet8 (struct tileset8 *tileset, struct tile8 *tile)
{
	if (!memcpy (&tileset->tiles[tileset->items], tile,
	             sizeof (struct tile8))) {
		printf ("Failed to copy tile\n");
		exit (0);
	}
	tileset->items++;
}

void
freeSet8 (struct tileset8 *tileset)
{
	free (tileset->tiles);
	tileset->tiles = NULL;
	tileset->n = tileset->items = 0;
}

void
resizeSet16 (struct tileset16 *tileset, uint32_t delta)
{
	if (tileset->n == 0) {
		tileset->n = delta;
		tileset->tiles = malloc (tileset->n * sizeof (struct tile16));
		if (!tileset->tiles) {
			printf ("Failed malloc\n");
			exit (1);
		}
	} else {
		tileset->n += delta;
		tileset->tiles = realloc (tileset->tiles,
		                          tileset->n * sizeof (struct tile16));
		if (!tileset->tiles) {
			printf ("Failed realloc\n");
			exit (1);
		}
	}
}

void
insertSet16 (struct tileset16 *tileset, struct tile16 *tile)
{
	if (!memcpy (&tileset->tiles[tileset->items], tile,
	             sizeof (struct tile16))) {
		printf ("Failed to copy tile\n");
		exit (0);
	}
	tileset->items++;
}

void
freeSet16 (struct tileset16 *tileset)
{
	free (tileset->tiles);
	tileset->tiles = NULL;
	tileset->n = tileset->items = 0;
}

void
fileToTile8 (unsigned char *file, uint32_t size, struct tileset8 *tileset)
{
	unsigned char batch;
	unsigned char x, y, b, bit, c;
	uint32_t offset = 0;
	uint32_t batches = 0;

	do {
		batch = file[0 + offset];
		if (file[1 + offset] != 0x01 && file[2 + offset] != 0x08) {
			printf ("Unexpected tile size\n");
			exit (0);
		}
		resizeSet8 (tileset, batch);
		for (b = 0; b < batch; b++) {
			for (y = 0; y < 8; y++) {
				for (x = 0; x < 1; x++) {
					for (bit = 0; bit < 8; bit++) {
						// Red
						c = 0x00;
						c |= 0x1
						     & (file[4 + offset]
						        >> bit);
						// Green
						c = c << 1;
						c |= 0x1
						     & (file[5 + offset]
						        >> bit);
						// Blue
						c = c << 1;
						c |= 0x1
						     & (file[6 + offset]
						        >> bit);
						// Intensity
						c = c << 1;
						c |= 0x1
						     & (file[7 + offset]
						        >> bit);

						tileset->tiles[b + batches]
							.pixel[x * 8 + bit][y]
							= CGApalette[c];
						// Alpha
						if (0x1
						    & (file[3 + offset] >> bit))
							tileset->tiles[b
							               + batches]
								.pixel[x * 8
							               + bit][y]
								|= 0xFF;
					}
					offset += 5;
				}
			}
		}
		if (offset % 2048) {
			offset = offset - (offset % 2048) + 2048;
		}
		batches += batch;
		tileset->items += batch;
	} while (offset < size);
}

void
fileToTile16 (unsigned char *file, uint32_t size, struct tileset16 *tileset)
{
	unsigned char batch;
	unsigned char x, y, b, bit, c;
	uint32_t offset = 0;
	uint32_t batches = 0;

	do {
		batch = file[0 + offset];
		if (file[1 + offset] != 0x02 && file[2 + offset] != 0x10) {
			printf ("Unexpected tile size\n");
			exit (0);
		}
		resizeSet16 (tileset, batch);
		for (b = 0; b < batch; b++) {
			for (y = 0; y < 16; y++) {
				for (x = 0; x < 2; x++) {
					for (bit = 0; bit < 8; bit++) {
						// Red
						c = 0x00;
						c |= 0x1
						     & (file[4 + offset]
						        >> bit);
						// Green
						c = c << 1;
						c |= 0x1
						     & (file[5 + offset]
						        >> bit);
						// Blue
						c = c << 1;
						c |= 0x1
						     & (file[6 + offset]
						        >> bit);
						// Intensity
						c = c << 1;
						c |= 0x1
						     & (file[7 + offset]
						        >> bit);

						tileset->tiles[b + batches]
							.pixel[x * 8 + bit][y]
							= CGApalette[c];
						// Alpha
						if (0x1
						    & (file[3 + offset] >> bit))
							tileset->tiles[b
							               + batches]
								.pixel[x * 8
							               + bit][y]
								|= 0xFF;
					}
					offset += 5;
				}
			}
		}
		if (offset % 8064) {
			offset = offset - (offset % 8064) + 8064;
		}
		batches += batch;
		tileset->items += batch;
	} while (offset < size);
}
