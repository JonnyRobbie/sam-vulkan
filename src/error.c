#include "error.h"
#include <SDL2/SDL.h>
#include <stdio.h>

int
printe (char *message)
{
	fprintf (stderr, "%s\n", message);
	fprintf (stderr, "SDL Error: %s\n", SDL_GetError ());
	return -1;
}
