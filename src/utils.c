#include "utils.h"
#include <math.h>
#include <stdio.h>

struct VkVersionNumber
getVersionFromVk (uint32_t version)
{
	struct VkVersionNumber ret;

	ret.major = (version & (uint32_t)0xFFC00000) >> 22;
	ret.minor = (version & (uint32_t)0x3FF000) >> 12;
	ret.patch = (version & (uint32_t)0xFFF);

	return ret;
}

void
printVersionFromVk (struct VkVersionNumber version)
{
	printf ("v%i.%i.%i", version.major, version.minor, version.patch);
}

void
printFlags (uint32_t flags)
{
	uint32_t mask = 2147483648;
	for (int i = 0; i < 32; i++) {
		if (flags & mask) {
			printf ("x");
		} else {
			printf ("-");
		}
		mask >>= 1;
	}
}

uint32_t
min (uint32_t a, uint32_t b)
{
	return a < b ? a : b;
}

uint32_t
max (uint32_t a, uint32_t b)
{
	return a > b ? a : b;
}
