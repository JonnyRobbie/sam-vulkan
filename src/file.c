#include <stdio.h>
#include <stdlib.h>

int
loadFile (const char *filename, unsigned char **result)
{
	// http://www.anyexample.com/programming/c/how_to_load_file_into_memory_using_plain_ansi_c_language.xml
	int size = 0;
	FILE *f = fopen (filename, "rb");
	if (f == NULL) {
		*result = NULL;
		return -1; // -1 means file opening fail
	}
	fseek (f, 0, SEEK_END);
	size = ftell (f);
	fseek (f, 0, SEEK_SET);
	*result = (unsigned char *)malloc (size + 1);
	if (size != fread (*result, sizeof (unsigned char), size, f)) {
		free (*result);
		return -2; // -2 means file reading fail
	}
	fclose (f);
	(*result)[size] = 0;
	return size;
}
