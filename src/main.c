#include "error.h"
#include "fileio.h"
#include "utils.h"
#include <SDL2/SDL.h>
#include <SDL2/SDL_image.h>
#include <SDL2/SDL_vulkan.h>
#include <stdio.h>
#include <sys/time.h>

#include "decode.h"
#include "file.h"
#include "graphics.h"
#include "level.h"
#include "samvk.h"

const int SCREEN_WIDTH = 320;
const int SCREEN_HEIGHT = 200;
const int PIXEL_SIZE = 4;
const float PIXEL_RATIO = 1.2;
const int PREFERRED_DEVICE = -1;

int
main (int argc, char *argv[])
{
	int i, j, k;
	// SDL initialize window and surfaces
	SDL_Window *gWin = NULL;
	SDL_Surface *gSurface = NULL;
	SDL_Surface *gBG = NULL;
	char gWinTitle[20] = "SAM Vulkan";

	SDL_SetHint (SDL_HINT_VIDEO_X11_NET_WM_BYPASS_COMPOSITOR, "0");
	if (SDL_Init (SDL_INIT_VIDEO) < 0) {
		return printe ("Failed to initialize SDL\n");
	}
	gWin = SDL_CreateWindow (
		gWinTitle, SDL_WINDOWPOS_UNDEFINED, SDL_WINDOWPOS_UNDEFINED,
		SCREEN_WIDTH * PIXEL_SIZE, SCREEN_HEIGHT * PIXEL_SIZE,
		SDL_WINDOW_SHOWN | SDL_WINDOW_VULKAN | SDL_WINDOW_RESIZABLE);
	if (gWin == NULL) {
		return printe ("Failed to create SDL window");
	}

	struct SAMVk samvk;
	if (SAMVkInit (&samvk, gWin, SCREEN_WIDTH, SCREEN_HEIGHT, PIXEL_SIZE,
	               PIXEL_RATIO, 1)) {
		printf ("Failed to initialize SAMVk\n");
		return -1;
	}

	Uint32 rmask, gmask, bmask, amask;
	rmask = 0xff000000;
	gmask = 0x00ff0000;
	bmask = 0x0000ff00;
	amask = 0x000000ff;

	// Load 16x16 tileset
	printf ("Loading big tilesets\n");
	unsigned char *cGFX1;
	unsigned int sizeGFX1;
	sizeGFX1 = loadFile ("SAM101.GFX", &cGFX1);
	if (sizeGFX1 < 0) {
		printf ("Error loading file 1\n");
		return -1;
	}
	printf ("Decoding big tilesets\n");
	SAMDecode (cGFX1, sizeGFX1, 8064);
	struct tileset16 tilesetBig;
	tilesetBig.n = 0;
	tilesetBig.items = 0;
	fileToTile16 (cGFX1, sizeGFX1, &tilesetBig);

	// Load 8x8 tileset
	printf ("Loading small tilesets\n");
	unsigned char *cGFX2;
	unsigned int sizeGFX2;
	sizeGFX2 = loadFile ("SAM102.GFX", &cGFX2);
	if (sizeGFX2 < 0) {
		printf ("Error loading file 1\n");
		return -1;
	}
	printf ("Decoding small tilesets\n");
	SAMDecode (cGFX2, sizeGFX2, 2048);
	struct tileset8 tilesetSmall;
	tilesetSmall.n = 0;
	tilesetSmall.items = 0;
	fileToTile8 (cGFX2, sizeGFX2, &tilesetSmall);

	// Load level data
	printf ("Loading levels\n");
	unsigned char *cGFX3;
	unsigned int sizeGFX3;
	sizeGFX3 = loadFile ("SAM103.GFX", &cGFX3);
	if (sizeGFX3 < 0) {
		printf ("Error loading file 3\n");
		return -1;
	}

	printf ("Decoding levels\n");
	SAMDecode (cGFX3, sizeGFX3, 42);
	int nLevel = 16;
	struct level level0;
	fileToLevel (cGFX3 + nLevel * 2016, &level0);
	/*
	 *printLevel(&level0);
	 */

	// Create tilesetAtlas for 8x8 tiles
	SDL_Surface *tileAtlasSmall;
	tileAtlasSmall = SDL_CreateRGBSurface (0, 128, 128, 32, rmask, gmask,
	                                       bmask, amask);
	if (tileAtlasSmall == NULL) {
		printf ("Atlas small creation failed: %s\n", SDL_GetError ());
	}

	printf ("Tileset 8x8 size: %i\n", tilesetSmall.items);
	for (char sy = 0; sy < 16; sy++) {
		for (char sx = 0; sx < 16; sx++) {
			for (char y = 0; y < 8; y++) {
				for (char x = 0; x < 8; x++) {
					((uint32_t *)tileAtlasSmall
					         ->pixels)[x + 128 * y + 8 * sx
					                   + 128 * 8 * sy]
						= tilesetSmall
					                  .tiles[sy * 16 + sx]
					                  .pixel[7 - x][y];
				}
			}
			if (sy * 16 + sx >= tilesetSmall.items)
				goto outTilesetSmall;
		}
	}
outTilesetSmall:;

	// Create tilesetAtlas for 16x16 tiles
	SDL_Surface *tileAtlasBig;
	tileAtlasBig = SDL_CreateRGBSurface (0, 512, 512, 32, rmask, gmask,
	                                     bmask, amask);
	if (tileAtlasBig == NULL) {
		printf ("Atlas big creation failed: %s\n", SDL_GetError ());
	}

	printf ("Tileset 16x16 size: %i\n", tilesetBig.items);
	for (char sy = 0; sy < 32; sy++) {
		for (char sx = 0; sx < 32; sx++) {
			for (char y = 0; y < 16; y++) {
				for (char x = 0; x < 16; x++) {
					((uint32_t *)tileAtlasBig
					         ->pixels)[x + 512 * y + 16 * sx
					                   + 512 * 16 * sy]
						= tilesetBig.tiles[sy * 32 + sx]
					                  .pixel[15 - x][y];
				}
			}
			if (sy * 32 + sx >= tilesetBig.items)
				goto outTilesetBig;
		}
	}
outTilesetBig:;

	/*goto quit;*/

	// Start polling with event loop
	SDL_Event e;
	char quit = 0;
	uint32_t fpsCount = 0;
	struct timeval fpsBegin, fpsEnd;
	gettimeofday (&fpsBegin, NULL);
	printf ("Entering loop...\n");
	while (!quit) {
		while (SDL_PollEvent (&e)) {
			switch (e.type) {
			case SDL_QUIT:
				quit = 1;
				break;
			case SDL_WINDOWEVENT:
				// EXPOSED window event
				// to handle redrawing
				// the window without
				// compositor
				// https://stackoverflow.com/q/47296294/1291302
				switch (e.window.event) {
				case SDL_WINDOWEVENT_EXPOSED:
					printf ("window EXPOSED\n");
					SDL_UpdateWindowSurface (gWin);
					break;
				case SDL_WINDOWEVENT_RESIZED:
					printf ("window RESIZED\n");
					samvk.framebufferResized = 1;
					break;
				}
				break;
			case SDL_KEYDOWN:
				printf ("Key %i\n", e.key.keysym.sym);
				// Q key
				if (e.key.keysym.sym == 113) {
					quit = 1;
					break;
				}
				/*case
				 * SDL_MOUSEMOTION:*/
				/*printf("Relative
				 * motion: %i, %i\n",
				 * e.motion.xrel,
				 * e.motion.yrel);*/
				/*break;*/
			}
		}
		SAMVkDrawFrame (&samvk);

		// FPS draw
		fpsCount++;
		gettimeofday (&fpsEnd, NULL);
		if (fpsEnd.tv_sec > fpsBegin.tv_sec
		    && fpsEnd.tv_usec > fpsBegin.tv_usec) {
			fpsBegin = fpsEnd;
			sprintf (gWinTitle, "FPS: %i", fpsCount);
			SDL_SetWindowTitle (gWin, gWinTitle);
			fpsCount = 0;
		}
	}
	printf ("...exiting loop.\n");
	vkDeviceWaitIdle (samvk.device);

quit:
	SAMVkDestruct (&samvk);
	SDL_DestroyWindow (gWin);
	SDL_Quit ();
	printf ("Gracefully quitting\n");

	return 0;
}
