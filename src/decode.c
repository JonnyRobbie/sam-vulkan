#include "decode.h"
#include <stdio.h>

int
SAMDecode (unsigned char *content, unsigned int size, unsigned int page)
{
	char key[] = "Copyright 1991 Peder Jungck";

	for (int i = 0; i < size; i++) {
		content[i] = (lookup[content[i] & 0b1111] << 4)
		             | lookup[content[i] >> 4];
		content[i] = content[i] ^ key[i % page % 28];
	}

	return 0;
}
