#ifndef GRAPHICS_H
#define GRAPHICS_H

#include <SDL2/SDL.h>
#include <inttypes.h>

struct tile8 {
	uint32_t pixel[8][8];
};

struct tile16 {
	uint32_t pixel[16][16];
};

struct background {
	uint32_t pixel[320][200];
};

struct tileset8 {
	struct tile8 *tiles;
	uint32_t items;
	uint32_t n;
};

struct tileset16 {
	struct tile16 *tiles;
	uint32_t items;
	uint32_t n;
};

void resizeSet8 (struct tileset8 *, uint32_t delta);
void resizeSet16 (struct tileset16 *, uint32_t delta);
void insertSet8 (struct tileset8 *, struct tile8 *tile);
void insertSet16 (struct tileset16 *, struct tile16 *tile);
void freeSet8 (struct tileset8 *);
void freeSet16 (struct tileset16 *);

void fileToTile8 (unsigned char *file, uint32_t size, struct tileset8 *tileset);
void fileToTile16 (unsigned char *file, uint32_t size,
                   struct tileset16 *tileset);

#endif
