#ifndef SAMVK_H
#define SAMVK_H

#include "fileio.h"
#include <SDL2/SDL.h>
#include <vulkan/vulkan.h>

struct SuitableSwapChain {
	VkSurfaceFormatKHR format;
	VkPresentModeKHR presentMode;
	VkExtent2D extent;
	uint32_t imageCount;
	VkSurfaceTransformFlagBitsKHR transform;
};

struct SAMVk {
	SDL_Window *gWin;
	int width;
	int height;
	uint32_t pixelSize;
	float pixelRatio;
	VkResult result;
	VkDevice device;
	VkPhysicalDevice physDevice;
	VkInstance instance;
	VkSurfaceKHR surface;
	struct SuitableSwapChain *suitableSwapChain;
	int32_t suitableDevice;
	VkQueue graphicsQueue;
	VkQueue presentQueue;
	uint32_t queueSet[2];
	size_t queueSetSize;
	VkSwapchainKHR swapChain;
	VkImage *swapChainImages;
	VkImageView *swapChainImageViews;
	VkShaderModule vertShaderModule;
	VkShaderModule fragShaderModule;
	struct BinFile vertShader;
	struct BinFile fragShader;
	VkRenderPass renderPass;
	VkPipelineLayout pipelineLayout;
	VkPipeline graphicsPipeline;
	VkFramebuffer *swapChainFramebuffers;
	VkCommandPool commandPool;
	VkCommandBuffer *commandBuffers;
	VkSemaphore *imageAvailableSemaphores;
	VkSemaphore *renderFinishedSemaphores;
	VkFence *inFlightFences;
	VkFence *imagesInFlight;
	uint32_t currentFrame;
	char framebufferResized;
};

int SAMVkCreateSwapchain (struct SAMVk *);

int SAMVkCleanupSwapchain (struct SAMVk *);

uint32_t SAMVkChooseSuitableSwapchain (struct SAMVk *samvk,
                                       int32_t suitableDevice,
                                       VkPhysicalDevice device);

int SAMVkRecreateSwapchain (struct SAMVk *);

int SAMVkInit (struct SAMVk *, SDL_Window *, uint32_t width, uint32_t height,
               uint32_t pixelSize, float pixelRatio, int preferredDevice);

int SAMVkDestruct (struct SAMVk *);

int SAMVkDrawFrame (struct SAMVk *);

#endif
