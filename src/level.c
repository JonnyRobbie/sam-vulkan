#include "level.h"
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

int
fileToLevel (unsigned char *file, struct level *level)
{
	char tileset[3];
	int x, y, yOffset = 0;
	memcpy (tileset, file, 0x03);
	// Store basic level properties
	level->tileset = atoi (tileset);
	level->bgOverlay = file[1 + 42];
	level->tOffset35 = file[4 + 42];
	level->tOffset36 = file[5 + 42];
	level->tOffset37 = file[6 + 42];

	// Main level data loop based on input rows
	for (y = 2; y < 48; y++) {
		// If tehe first two bytes of a line are \cr\lf, store
		// level heght
		if (file[42 * y] == 0x0d && file[1 + 42 * y] == 0x0a) {
			level->height = y - yOffset - 2;
		}

		// If the forst byte is '*', write overlay data
		if (file[42 * y] == 0x2a) {
			yOffset++;
			for (x = 0; x < 40; x++) {
				level->overlay[x][y - yOffset - 2]
					= file[x + 42 * y];
			}
		} else {
			for (x = 0; x < 40; x++) {
				level->tile[x][y - yOffset - 2]
					= file[x + 42 * y];
				level->overlay[x][y - yOffset - 2] = 0x00;
			}
		}
	}

	// Clean (zero-out) lines left over
	for (y = 46 - yOffset; y < 46; y++) {
		for (x = 0; x < 40; x++) {
			level->tile[x][y] = 0x00;
			level->overlay[x][y] = 0x00;
		}
	}
	return 0;
}

void
printLevel (struct level *level)
{
	int x, y;
	printf ("Tileset: %i\nOverlay tile: 0x%.2x\nTile offsets: 0x%.2x, "
	        "0x%.2x, "
	        "0x%.2x\n",
	        level->tileset, level->bgOverlay, level->tOffset35,
	        level->tOffset36, level->tOffset37);
	printf ("Level height: %i\n", level->height);

	printf ("Tiles:\n");
	for (y = 0; y < 46; y++) {
		for (x = 0; x < 40; x++) {
			printf ("%.2x ", level->tile[x][y]);
		}
		printf ("\n");
	}

	printf ("Overlay tiles:\n");
	for (y = 0; y < 46; y++) {
		for (x = 0; x < 40; x++) {
			printf ("%.2x ", level->overlay[x][y]);
		}
		printf ("\n");
	}
}
