#ifndef LEVEL_H
#define LEVEL_H

struct level {
	unsigned int tileset;
	unsigned char bgOverlay;
	unsigned char tOffset35;
	unsigned char tOffset36;
	unsigned char tOffset37;
	unsigned char tile[40][46];
	unsigned char overlay[40][46];
	unsigned char height;
};

int fileToLevel (unsigned char *file, struct level *level);

void printLevel (struct level *level);

#endif
