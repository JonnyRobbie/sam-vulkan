CC=gcc
CFLAGS=-I.
DEPS = decode.h file.h level.h graphics.h error.h fileio.h utils.h samvk.h
_OBJ = decode.o file.o level.o graphics.o error.o fileio.o utils.o samvk.o
OBJ = $(patsubst %,src/%,$(_OBJ))

LIBS =-lSDL2 -lSDL2_image -lvulkan

SPIR = shaders/vert.spv shaders/frag.spv

release: CFLAGS += -DNDEBUG
release: bin/main

debug: CFLAGS += -g
debug: bin/main-debug

bin/main: src/main.c $(OBJ) $(SPIR)
	$(CC) -o bin/main src/main.c $(OBJ) $(CFLAGS) $(LIBS)

bin/main-debug: src/main.c $(OBJ) $(SPIR)
	$(CC) -o bin/main-debug src/main.c $(OBJ) $(CFLAGS) $(LIBS)

%.o: %.c %.h
	$(CC) -c -o $@ $< $(CFLAGS)

$(SPIR):
	glslc -o $@ $(patsubst shaders/%.spv, shaders/shader.%, $@)

clean:
	rm -f src/*.o
	rm -f shaders/*.spv
	rm -f bin/*

format:
	clang-format --style=file -i src/*.c src/*.h
